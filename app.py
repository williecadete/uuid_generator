# encoding: utf-8

from flask import Flask
import uuid
import redis

app = Flask(__name__)

db = redis.Redis('redis') #connect to server

@app.route('/')
def home():
    u = str(uuid.uuid4())
    db.set(u,u)
    return str(db.keys())